# MetaRouter Public Docs

[![pipeline status](https://gitlab.com/metarouter-public/docs/badges/master/pipeline.svg)](https://gitlab.com/metarouter-public/docs/-/commits/master)
[![generated via gatsby](https://img.shields.io/badge/generated%20via-gatsby-blue)](https://www.gatsbyjs.org/)
[![styled with tailwind css](https://img.shields.io/badge/styled%20with-tailwind%20css-blue)](https://tailwindcss.com/)
[![built by gitlab ci/cd](https://img.shields.io/badge/built%20by-gitlab%20ci%2Fcd-blue)](https://gitlab.com/metarouter-public/docs/pipelines)
[![hosted on gitlab pages](https://img.shields.io/badge/hosted%20on-gitlab%20pages-blue)](https://gitlab.com/metarouter-public/docs/pages)




## Editing Content

All of the content for this project is located in the [src/content](https://gitlab.com/metarouter-public/docs/-/tree/master/src/content) directory. The URL structure, navigation, and SEO optimizations for the documentation website are generated directly from the metadata (frontmatter) and content of the markdown files in this directory. View some of the existing markdown files to see how things are structured, and see the following links for more detail:

- [Content Formatting](https://gitlab.com/metarouter-public/docs/-/blob/master/readme/markdown-formatting.md)
- [Frontmatter Fields](https://gitlab.com/metarouter-public/docs/-/blob/master/readme/markdown-frontmatter.md)

### Creating a New Page / Markdown File

When adding a new markdown file within the **/src/content** directory, note that the file's physical location within this directory is independent of the route visitors will use to access the corresponding content. The public URL is specified by the [path](https://gitlab.com/metarouter-public/docs/-/blob/master/readme/markdown-frontmatter.md#path-string-required) field of the markdown file's frontmatter.

After your new page is merged to master, it will be deployed to the live site and can be viewed at `docs.metarouter.io/<path>`.

### Previewing Changes

If you'd like to preview the output of a markdown file before committing changes, https://dillinger.io/ is a good option to verify things look roughly as expected.

## Development

### Install Gatsby CLI

```
npm install -g gatsby-cli
cd docs-gatsby
yarn install
```

Run `gatsby develop`. Try running `gatsby clean` first if you encounter errors.

### Build & Run Locally

Run `gatsby build && gatsby serve`.

## Live Environment & Deployment

The live site is hosted on GitLab pages, accessible at [docs.metarouter.io](https://docs.metarouter.io). Changes merged to master are automatically deployed via GitLab CI.

## Site Search

Site search functionality (autocomplete, indexing and results) is provided by Algolia's free [DocSearch](https://docsearch.algolia.com/) program. Content is indexed every 24 hours.

Config for the search index lives at https://github.com/algolia/docsearch-configs/blob/master/configs/metarouter.json. This [config can be edited](https://docsearch.algolia.com/docs/required-configuration) via pull request to tune search indexing and results.

- [Algolia Dashboard](https://www.algolia.com/apps/A9560YI10O/dashboard)
- [Google Search Console](https://search.google.com/search-console/)

## URL Redirects from the Previous Version

URL redirects from the [previous version](https://gitlab.com/metarouter/documentation) of the public docs are handled by creating a markdown file for each URL in `/src/content/_redirects`.

The markdown file requires two fields in its frontmatter specifying the original path and the new URL that should handle the request instead.

```
---
path: /v2/editions/cloud/overview.html
redirectTo: /cloud-edition/
---
```

These files are then handled by the **redirect.js** template that uses a meta refresh to route users and search engines to the correct destination. The meta refresh is used to due to limited options for redirects on the GitLab pages platform.
