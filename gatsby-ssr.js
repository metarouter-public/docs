import React from 'react'

export const onPreRenderHTML = ({ getHeadComponents, replaceHeadComponents }) => {
  if (process.env.NODE_ENV !== 'production') {
    return
  }
  const headComponents = getHeadComponents()

  // workaround to revert forcefully injected inline styles
  // described in https://github.com/gatsbyjs/gatsby/issues/1526
  // takes data-href from <style> tag with inline styles which contains URL to global css file
  // and transforms it into stylesheet link component
  const transformedHeadComponents = headComponents.map(node => {
    if (node.type === 'style') {
      const globalStyleHref = node.props['data-href']
      if (globalStyleHref) {
        return <link href={globalStyleHref} rel="stylesheet" type="text/css" />
      } else {
        return node
      }
    } else {
      return node
    }
  })
  replaceHeadComponents(transformedHeadComponents)
}

// analytics scripts for prod builds
const analyticsScripts =
  process.env.NODE_ENV !== 'production'
    ? []
    : [
        <script
          key="analytics.js"
          dangerouslySetInnerHTML={{
            __html: `
        !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Astronomer snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t){var e=document.createElement("script");e.type="text/javascript";e.async=!0;e.src=("https:"===document.location.protocol?"https://":"http://")+"cdn.astronomer.io/analytics.js/v1/"+t+"/"+t+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(e,n)};analytics.SNIPPET_VERSION="3.1.0";
        analytics.load("tKxWJZiGTYgskKwEduC6D");
        analytics.page()
        }}();
      `,
          }}
        />,
      ]

const hubbot = [
  <script key="hs-script-loader" id="hs-script-loader" async defer src="//js.hs-scripts.com/6977481.js"></script>,
]

export const onRenderBody = ({ setPostBodyComponents }) => {
  setPostBodyComponents([...analyticsScripts, ...hubbot])
}
