---
collectionKey: sources

navText: Mobile Spec

path: '/sources/mobile-spec/'

tags: [sources]
---

# Native Mobile Spec

MetaRouter's private cloud data routing and server-side integration
architecture is built primarily to accept the open-source Analytics.js
tracking schema created and maintained by Segment. On their
<a href="https://segment.com/docs/connections/spec/mobile"
target="_blank">documentation site</a>, Segment defines a clear and
complete specification for mobile application events, referenced
below. MetaRouter leverages and fully supports this specification and
Segment's open-source libraries for [Android](/sources/android/) and
[iOS](/sources/ios/).

## Overview of Events

**Application Lifecycle Events**

- <a href="https://segment.com/docs/connections/spec/mobile#application-installed" target="_blank">Application Installed</a>
- <a href="https://segment.com/docs/connections/spec/mobile#application-opened" target="_blank">Application Opened</a>
- <a href="https://segment.com/docs/connections/spec/mobile#application-updated" target="_blank">Application Updated</a>
- <a href="https://segment.com/docs/connections/spec/mobile#application-backgrounded" target="_blank">Application Backgrounded</a>
- <a href="https://segment.com/docs/connections/spec/mobile#application-crashed" target="_blank">Application Crashed</a>
- <a href="https://segment.com/docs/connections/spec/mobile#application-uninstalled" target="_blank">Application Uninstalled</a>

**Campaign Events**

- <a href="https://segment.com/docs/connections/spec/mobile#push-notification-received" target="_blank">Push Notification Received</a>
- <a href="https://segment.com/docs/connections/spec/mobile#push-notification-tapped" target="_blank">Push Notification Tapped</a>
- <a href="https://segment.com/docs/connections/spec/mobile#push-notification-bounced" target="_blank">Push Notification Bounced</a>
- <a href="https://segment.com/docs/connections/spec/mobile#install-attributed" target="_blank">Install Attributed</a>
- <a href="https://segment.com/docs/connections/spec/mobile#deep-link-clicked" target="_blank">Deep Link Clicked</a>
- <a href="https://segment.com/docs/connections/spec/mobile#deep-link-opened" target="_blank">Deep Link Opened</a>
