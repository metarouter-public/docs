---
collectionKey: cloud-destinations

navText: LinkedIn Insight Tag

path: '/cloud-destinations/linkedin-insight-tag/'

tags: [cloud, destinations]
---

# LinkedIn Insight Tag

MetaRouter simplifies your website's tag ecosystem by launching the [LinkedIn Insight Tag](https://business.linkedin.com/marketing-solutions/insight-tag), without having to install LinkedIn's javascript tag. Once you complete the steps below, MetaRouter will automatically fire the Insight tag on your website.

## What is LinkedIn and how does it work?

LinkedIn is a social media platform that is primarily oriented towards business professionals who are interested in networking with others in their industry, finding sales prospects and jobs, and more. This presents a great opportunity for marketers who would like to reach specific professional audiences via targeted advertising.
LinkedIn enables advertising by targeting users based on their industry, job title, location, etc. Ads are placed in various locations on the platform, such as in their news feed or in LinkedIn mail that is sent directly to users via its messaging capability. 

The platform also allows you to analyze your website traffic and ads interactions to inform targeting via the Insight Tag, which is a lightweight javascript snippet. 
By installing the Insight Tag with MetaRouter, you enable LinkedIn to provide you with information about the types of users on your website, as well as place conversion events based on page URLs.

Learn more about the [LinkedIn Insight Tag](https://www.linkedin.com/help/lms/answer/65521) and [Conversion Tracking](https://www.linkedin.com/help/lms/answer/67514). 

## Why send data to LinkedIn using MetaRouter

By integrating your LinkedIn Insight Tag with MetaRouter, you no longer have to manage the tag on your own. Instead, you can control the tag within MetaRouter, allowing your marketing and analytics teams to turn the destination on and off with a few easy clicks. Once the tag is enabled, you'll begin collecting insights automatically, and can activate conversions within your LinkedIn  Marketing Solutions account.

**Please note that the LinkedIn destination does not utilize MetaRouter API calls such as `track` , `page`, and `identify`. Instead, it automatically collects this information for you via the Insight Tag.**

## Getting Started with LinkedIn and MetaRouter

Before continuing, make sure that any Insight Tags you have previously installed are removed from your website.

### LinkedIn Side

Once you create a LinkedIn Campaign Manager account, you'll need to find your Partner Id. To do this:
1. Login to Campaign Manager
2. Select the account you'd work with
3. Click the Account Assets drop-down & select Insight Tag:

![linkedin1](/images/linkedin1.png)

4. Once you reach the Website Tracking page, click the Manage Insight Tag drop-down and select "See Tag":

![linkedin2](/images/linkedin2.png)

5. Once on the Install my Insight Tag page, click the "I will use a tag manager" drop-down. Copy your Partner Id: 

![linkedin3](/images/linkedin3.png)

### MetaRouter Side

Now that you have your Partner Id, follow the steps below to complete a LinkedIn destination setup in MetaRouter.

1. Login to your MetaRouter account and navigate to the client-side source you'd like to use to send data to LinkedIn.
2. Click on the Destinations tab and click the New Destination button.
3. Using the search bar, search for LinkedIn and select the LinkedIn Insight Tag destination. 
4. Enter a friendly destination name, friendly connection name and the Partner Id you copied earlier:

![linkedin4](/images/linkedin4.png)

5. Save the destination.

Within minutes you should be collecting customer data from your website and generating audience insights within LinkedIn. You'll also be able to track conversion events based on page URLs, which you can specify by clicking the "Account Assets" drop-down and selecting the "Conversions" button. 