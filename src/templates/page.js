import React from 'react'
import { graphql } from 'gatsby'

import SEO from '../components/seo'
import Layout from '../components/layout/layout'

export default ({ data, pageContext }) => {
  const { frontmatter } = data.markdownRemark
  const getTitle = function() {
    if (frontmatter.tags.includes('cloud') && frontmatter.tags.includes('destinations')) {
      return `${frontmatter.navText} - Cloud Destinations`
    }

    if (frontmatter.tags.includes('enterprise') && frontmatter.tags.includes('destinations')) {
      return `${frontmatter.navText} - Enterprise Destinations`
    }

    if (frontmatter.tags.includes('sources')) {
      return `${frontmatter.navText} - Sources & SDKs`
    }

    return null
  }

  return (
    <Layout>
      <SEO title={frontmatter.seoTitle || getTitle() || frontmatter.navText} />

      <div dangerouslySetInnerHTML={{ __html: pageContext.html }} />
    </Layout>
  )
}

export const pageQuery = graphql`
  query($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      frontmatter {
        path
        navText
        seoTitle
        tags
      }
    }
  }
`
