# Markdown Frontmatter Fields

These metadata fields are used to generate the static output of the public website.

## `collectionIndex` (Number, required)

Determines the file's alphanumeric sort order in navigation, relative to other files with the same `collectionKey`. Omit this field to fallback to sorting by `navText`.

For example, files with the key "cloud-destinations" do not use `collectionIndex` and are sorted by `navText` with the exception of the **Overview** which has `collectionIndex` set to `0` in order to appear as the first item in the navigation group.

![](/screenshots/nav-sort.png)

(This field is not required for the root path.)

## `collectionKey` (String, optional)

Markdown files sharing the same `collectionKey` are displayed together in navigation under the first file's `collectionTitle`.

## `collectionMerge` (Bool, optional)

Only applies to files with a `collectionIndex` of `0`.

When the first file in the collection has `collectionMerge` set to `true`, markdown files sharing the same `collectionKey` are merged into a single page, with order determined by each file's respective `collectionIndex`. The resulting page is then available at the path specified by the file in the collection with its `collectionIndex` set to `0`.

## `collectionTitle` ( String, optional)

Only applies to files with a `collectionIndex` of `0`, where it is required. Displayed as the collection heading in navigation. For example:

```
collectionIndex: 0
collectionTitle: Enterprise Destinations
```

Output:

![](/screenshots/collection-title.png)

## `navText` (String, required)

Displayed as link text in main navigation.

## `path` (String, required)

Public URL path used to view the markdown file content. May be different than the markdown filename. May contain slashes to create a nested URL structure.

## `seoTitle` (String, optional)

Content of the HTML `<title>` element. `navText` is used as default content if `seoTitle` is not set.

## `tags` (Array, optional)

Only applies to destinations and sources. Displayed as pills under the page's main heading.

Supported values are: `cloud`, `enterprise`, `destinations`, `sources`. For example:

```
tags: [enterprise, destinations]
```

Output:

![](/screenshots/tags.png)
