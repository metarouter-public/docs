# Markdown Content Formatting

- [Inline Formatting](#inline-formatting)
- [Code Blocks](#code-blocks)
- [Images](#images)
- [Tables](#tables)
- [Headings](#headings)

## Inline Formatting

- Use "double quotes" to indicate literal string values.
- Use **bold** text to indicate UI input names or navigation paths.
- Use `code` for inline code examples and to indicate variable or field names from source code.
- Use _italic text_ sparingly as it does not have strong visual contrast from default text. Works better when applied to a paragraph or longer passage than individual words.
- Capitalize Analytics.js except when used as a literal file path.
- Avoid inline markup and styles unless absolutely necessary.

## Code Blocks

Code blocks should specify a language in order to utilize syntax highlighting and be labeled correctly. See the list of languages supported by [Prism.js](https://prismjs.com/#supported-languages). For example:

````
```javascript
analytics.track(event, [properties], [options], [callback])
```
````

Special cases:

- Specify `csharp` as the language to output **.NET** as the label.
- Specify `bash` for command line examples. Displayed without a label.

## Images

For development, images should be added to the **/src/static/images** directory and referenced in Markdown from the public path **/images**. For example:

```md
![Segment Webhook](/images/segment-webhook-1.png)
```

Provide meaningful alt text when possible for additional SEO.

In production, images are copied to, and and served from cdn.metarouter.io.

### Linking Images

Images are automatically linked to their fullsize version during the build step. Images that are linked in the markdown content are excluded from auto-linking and retain their original links as authored. To manually exclude an image from auto-linking, use an HTML image tag with the `nolink` class. Examples below.

Will be auto-linked:

```md
![some alt text](/images/aa_test1.png)
```

Will be excluded by `nolink` class:

```html
<img src="/images/aa_test1.png" class="nolink" alt="some alt text" />
```

Will be excluded by markdown link:

```md
[![some alt text](/images/aa_test1.png)](/local-path/or/absolute-url)
```

## Tables

### Text Alignment

Text within columns can be left, right, or center aligned based on the header row, see: https://help.github.com/en/github/writing-on-github/organizing-information-with-tables#formatting-content-within-your-table

### Column Widths

Percentage-based column widths can be set by adding a row immediately after the heading row, specifying each column's percentage width. Must total 100. See the example below. Columns default to equal widths if not set explicitly.

### Example Text Alignment and Column Width

The following Markdown:

```
|                 | Client | Server | Mobile |
| :-------------- | :----: | :----: | :----: |
| [40]            |  [20]  |  [20]  |  [20]  |
| Acquisio        |   ✔    |   x    |   x    |
| Adobe Analytics |   ✔    |   x    |   x    |
| Amazon Kinesis  |   ✔    |   ✔    |   ✔    |
```

will produce the following HTML:

```html
<table>
  <thead>
    <tr>
      <th align="left" style="width: 40%;"></th>
      <th align="center" style="width: 20%;">Client</th>
      <th align="center" style="width: 20%;">Server</th>
      <th align="center" style="width: 20%;">Mobile</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td align="left">Acquisio</td>
      <td align="center">✔</td>
      <td align="center">x</td>
      <td align="center">x</td>
    </tr>
    <tr>
      <td align="left">Adobe Analytics</td>
      <td align="center">✔</td>
      <td align="center">x</td>
      <td align="center">x</td>
    </tr>
    <tr>
      <td align="left">Amazon Kinesis</td>
      <td align="center">✔</td>
      <td align="center">✔</td>
      <td align="center">✔</td>
    </tr>
  </tbody>
</table>
```

and the following browser output:

![](/screenshots/table.png)

## Headings

- Each markdown file should have a 1st level heading as the first content following its frontmatter.
- Do not use fifth and sixth level headings.

### Table of Contents

Table of Contents navigation is generated for each markdown file from its 2nd level headings.

```
## A Second Level Heading
```

Edit the file's heading levels as desired to include or exclude headings from its Table of Contents navigation. Example output:

![](/screenshots/table-of-contents.png)
