/* global docsearch */

exports.onRouteUpdate = () => {
  var navList = document.querySelector('.nav-list')
  var navDropdown = document.querySelector('.nav-dropdown select')
  var currentNavSelector = 'selected-item'
  var sectionAnchorSelector = 'section-anchor' // <h1> with this class indicates the page is made of merged markdown files
  var documentElement = document.documentElement

  var throttle = function(callback, limit) {
    var wait = false
    return function() {
      if (!wait) {
        callback.call()
        wait = true
        setTimeout(function() {
          wait = false
        }, limit)
      }
    }
  }

  var inViewport = function(el, offsetTop) {
    offsetTop = offsetTop || 0

    var rect = el.getBoundingClientRect()

    return (
      rect.top >= 0 + offsetTop &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || documentElement.clientWidth)
    )
  }

  var onClickNav = function(e) {
    var tag = e.target.tagName.toLowerCase()

    // toggle section when header is clicked
    if (tag === 'h2') {
      e.target.classList.toggle('collapsed')
      e.target.nextSibling.classList.toggle('hidden')
      setNavCollapsedState()
    } else if (tag === 'a' && !e.target.classList.contains('logo')) {
      // save the navigation scroll position
      sessionStorage.setItem('navStateScroll', navList.querySelector('.scrollbox').scrollTop)

      navList.querySelectorAll('li a').forEach(function(a) {
        a.classList.remove(currentNavSelector)
      })
      e.target.classList.add(currentNavSelector)
    }
  }

  // find the parent heading for the subheading, get its internal anchor and highlight the corresponding menu item
  var setCurrentForSubheading = function() {
    var subheading = document.querySelector('main a[href="' + window.location.hash + '"]')

    if (subheading) {
      var parentHeading = subheading.closest('section').querySelector('h1')
      var navItem
      if (parentHeading.querySelector('a[href]')) {
        var url = new URL(parentHeading.querySelector('a').href)
        navItem = navList.querySelector('li a[href="' + url.pathname + url.hash + '"]')
      }

      // hash selector will not match any items in the nav list when it belongs to standalone (versus multi-section) page
      // fallback to the current pathname to find corresponding nav item
      if (!navItem) {
        navItem = navList.querySelector('li a[href="' + window.location.pathname + '"]')
      }
      navItem.classList.add(currentNavSelector)
    }
  }

  var setCurrent = function() {
    if (navList) {
      var href = window.location.pathname + window.location.hash
      var anchor = navList.querySelector('li a[href="' + href + '"]')

      navList.querySelectorAll('a').forEach(function(a) {
        a.classList.remove(currentNavSelector)
      })

      // check if an anchor with the current path exists in nav list
      // will be false for subheading anchors
      if (anchor) {
        anchor.classList.add(currentNavSelector)
      } else {
        setCurrentForSubheading()
      }

      var selectedDropdownIndex = Array.from(navDropdown.options).findIndex(function(option) {
        return option.value === href
      })

      navDropdown.selectedIndex = selectedDropdownIndex
    }
  }

  var lastSectionHash = null

  var setHashByOffset = function(sections) {
    if (!document.querySelector('.' + sectionAnchorSelector)) {
      return // bail out if this is a single page
    }

    sections.forEach(function(section) {
      var hasHash = window.location.hash !== ''
      var isSectionSubheading = false

      // check whether the current hash belongs to a subheading (h2, h3, etc.) in this section
      if (hasHash) {
        var currentEl = document.getElementById(window.location.hash.substring(1))
        isSectionSubheading =
          currentEl && section.contains(currentEl) && !currentEl.classList.contains(sectionAnchorSelector)
      }

      var id = section.querySelector('.' + sectionAnchorSelector).id

      if (
        lastSectionHash !== id &&
        !isSectionSubheading && // prevents hash change to section anchor when clicking one of its subheading links
        section.offsetTop < window.pageYOffset &&
        section.offsetTop + section.offsetHeight > window.pageYOffset
      ) {
        window.history.replaceState({}, '', '#' + id)
        lastSectionHash = id
      }
    })

    setCurrent()
  }

  // highlights the link in TOC for the associated content based on offset
  var setTocByOffset = function(headings) {
    if (!document.querySelector('.toc') || documentElement.clientWidth < 1280) {
      return
    }

    var atTop = documentElement.scrollTop < 150
    var atBottom = documentElement.scrollHeight - documentElement.scrollTop - documentElement.clientHeight < 100
    var unsetCurrent = function() {
      document.querySelectorAll('.toc a').forEach(function(a) {
        a.classList.remove('font-bold')
      })
    }

    // highlight the first heading
    if (atBottom) {
      unsetCurrent()
      var links = document.querySelectorAll('.toc a')
      links[links.length - 1].classList.add('font-bold')
      return
    }

    // highlight the last heading
    else if (atTop) {
      unsetCurrent()
      document.querySelector('.toc a').classList.add('font-bold')
      return
    }

    // highlight the heading that's closest to the top edge of the viewport
    else {
      headings.forEach(function(heading) {
        if (heading.getBoundingClientRect().top < 100) {
          // highlight the corresponding link in table of contents
          var url = new URL(heading.querySelector('.anchor').href)
          var tocLink = heading.closest('section').querySelector('.toc a[href="' + url.hash + '"]')

          unsetCurrent()
          tocLink.classList.add('font-bold')
          return
        }
      })
    }
  }

  var onScroll = throttle(function() {
    setHashByOffset(document.querySelectorAll('main section'))
    setTocByOffset(document.querySelectorAll('main h2'))
  }, 100)

  // add cloud green for table checkmarks
  var styleTableCells = function() {
    var cells = document.querySelectorAll('main table td')

    cells.forEach(function(cell) {
      if (cell.innerText === '✔') {
        cell.style.color = '#5fa284'
      }
    })
  }

  var setNavCollapsedState = function() {
    var collapsedGroups = navList.querySelectorAll('.scrollbox ul.hidden')
    var collapsed = collapsedGroups.length
      ? Array.from(collapsedGroups).map(function(group) {
          return group.dataset.title
        })
      : []

    sessionStorage.setItem('navStateCollapsed', collapsed.join(','))
  }

  // get nav state from session storage
  var syncNavState = function() {
    if (navList) {
      var titles = sessionStorage.getItem('navStateCollapsed')
      var scrollPosition = Number(sessionStorage.getItem('navStateScroll'))
      var currentNav = navList.querySelector('.' + currentNavSelector)
      var navHeaderHeight = navList.querySelector('header').clientHeight

      if (titles) {
        titles.split(',').forEach(function(title) {
          navList.querySelector(`[data-title=${title}]`).classList.add('hidden')
        })
      }

      if (scrollPosition) {
        navList.querySelector('.scrollbox').scrollTo(0, scrollPosition)
      }

      if (currentNav && !inViewport(currentNav, navHeaderHeight)) {
        navList.querySelector('.scrollbox').scrollTo(0, currentNav.offsetTop - navHeaderHeight)
      }
    }
  }

  var setStickyNav = function() {
    var isSticky = documentElement.clientWidth > 1280 // corresponds with CSS breakpoint

    if (!isSticky) {
      document.querySelectorAll('.toc a').forEach(function(a) {
        a.classList.remove('font-bold')
      })
    }
  }

  var onNavDropdownChange = function(e) {
    window.location.href = e.target.value
  }

  // https://remysharp.com/2007/04/12/how-to-detect-when-an-external-library-has-loaded
  var loadScript = function(src, test, callback) {
    var s = document.createElement('script')
    s.src = src
    document.body.appendChild(s)

    var callbackTimer = setInterval(function() {
      var call = false
      try {
        call = test.call()
      } catch (e) {}

      if (call) {
        clearInterval(callbackTimer)
        callback.call()
      }
    }, 100)
  }

  // wait until external script is loaded (and DOM is ready in dev) before calling docsearch
  var initSearch = function() {
    var isLoaded = function() {
      return typeof window.docsearch !== 'undefined' && document.getElementById('search-input')
    }

    var init = function() {
      docsearch({
        apiKey: 'd0d6fa7e88311d0eb85d41d6e9d09f19',
        indexName: 'metarouter',
        inputSelector: '#search-input',
        debug: false, // Set debug to true if you want to inspect the dropdown
      })
      document.getElementById('search-input').disabled = ''
    }

    loadScript('https://cdn.jsdelivr.net/npm/docsearch.js@2/dist/cdn/docsearch.min.js', isLoaded, init)
  }

  // functions to run on route update
  setCurrent()
  syncNavState()
  styleTableCells()
  setStickyNav()
  initSearch()

  // event handlers
  window.addEventListener('resize', setStickyNav)
  document.addEventListener('scroll', onScroll)
  navList && navList.addEventListener('click', onClickNav)
  navDropdown && navDropdown.addEventListener('change', onNavDropdownChange)
}
